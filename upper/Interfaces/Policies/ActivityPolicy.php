<?php

declare(strict_types=1);


namespace Upper\Interfaces\Policies;


use Upper\Domain\Entities\Activity\Activity;
use Upper\Domain\Entities\User;

class ActivityPolicy
{
    public function userSee(User $user, Activity $activity): bool
    {
        return $user->canSeeActivity($activity);
    }
}
