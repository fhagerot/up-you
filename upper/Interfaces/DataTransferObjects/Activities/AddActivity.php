<?php

declare(strict_types=1);


namespace Upper\Interfaces\DataTransferObjects\Activities;


use Illuminate\Validation\Rule;
use Upper\Domain\Entities\Activity\Activity;
use Upper\Infrastructure\BaseDto;

class AddActivity extends BaseDto
{
    public function rules(): array
    {
        return [
            'type' => ['required', Rule::in(Activity::AVAILABLE_TYPES)],
            'name' => 'required|string|min:3|max:40'
        ];
    }

    private $type;

    private $name;

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): void
    {
        $this->type = $type;
    }
}
