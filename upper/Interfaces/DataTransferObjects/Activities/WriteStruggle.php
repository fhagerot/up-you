<?php

declare(strict_types=1);


namespace Upper\Interfaces\DataTransferObjects\Activities;


use Upper\Infrastructure\BaseDto;

class WriteStruggle extends BaseDto
{
    public function rules(): array
    {
        return [
            'yes' => 'required|boolean',
        ];
    }

    private $yes;

    public function getYes(): bool
    {
        return $this->yes;
    }

    public function setYes(bool $yes): void
    {
        $this->yes = $yes;
    }
}
