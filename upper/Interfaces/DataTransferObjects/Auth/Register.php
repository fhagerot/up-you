<?php

declare(strict_types=1);


namespace Upper\Interfaces\DataTransferObjects\Auth;


use Upper\Infrastructure\BaseDto;

class Register extends BaseDto
{
    public function rules(): array
    {
        return [
            'email' => 'required|email',
            'password' => 'required|string',
            'name' => 'required|string'
        ];
    }

    private $email;

    private $password;

    private $name;

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }
}

