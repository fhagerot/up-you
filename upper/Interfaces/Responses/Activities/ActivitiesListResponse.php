<?php

declare(strict_types=1);


namespace Upper\Interfaces\Responses\Activities;


use Illuminate\Contracts\Support\Responsable;

class ActivitiesListResponse implements Responsable
{
    private $activities;

    public function __construct($activities)
    {
        $this->activities = $activities;
    }

    /**
     * Create an HTTP response that represents the object.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function toResponse($request)
    {
        return response()->json([
            $this->activities
        ]);
    }
}
