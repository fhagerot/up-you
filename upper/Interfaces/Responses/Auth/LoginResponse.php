<?php

declare(strict_types=1);


namespace Upper\Interfaces\Responses\Auth;


use Illuminate\Contracts\Support\Responsable;

class LoginResponse implements Responsable
{
    private $token;

    public function __construct(string $token)
    {
        $this->token = $token;
    }

    /**
     * Create an HTTP response that represents the object.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function toResponse($request)
    {
        return response()->json([
            'token' => $this->token
        ]);
    }
}
