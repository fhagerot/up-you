<?php

declare(strict_types=1);


namespace Upper\Interfaces\Http;


use Illuminate\Foundation\Support\Providers\RouteServiceProvider;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Route;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Upper\Domain\Entities\Activity\Activity;
use Upper\Domain\Repositories\ActivitiesRepository;
use Upper\Interfaces\Policies\ActivityPolicy;

class HttpServiceProvider extends RouteServiceProvider
{
    public function boot(): void
    {
        $this->bootPolicies();
        $this->bootRoutes();
        $this->bootRouteBindings();
    }

    private function bootPolicies(): void
    {
        Gate::policy(Activity::class, ActivityPolicy::class);
    }

    private function bootRoutes(): void
    {
        $this->routes(function () {
            Route::prefix('api')
                ->middleware('api')
                ->group(base_path('upper/Interfaces/Http/Api/routes.php'));
        });
    }

    private function bootRouteBindings(): void
    {

        Route::bind('activity', function ($value) {
            /** @var ActivitiesRepository $repository */
            $repository = $this->app->make(ActivitiesRepository::class);
            $activity = $repository->findByActivityIdAndUserId((int)$value, $this->app['request']->user()->id);

            if ($activity) {
                return $activity;
            }

            throw new NotFoundHttpException('Activity not found');
        });
    }
}
