<?php

declare(strict_types=1);


namespace Upper\Interfaces\Http\Api\Actions\Auth;


use Tymon\JWTAuth\JWTAuth;
use Upper\Domain\Exceptions\UnauthorizedException;
use Upper\Interfaces\DataTransferObjects\Auth\Login;
use Upper\Interfaces\Responses\Auth\LoginResponse;

class LoginAction
{
    public function __invoke(Login $dto)
    {
        $token = $this->getTokenByDto($dto);

        return new LoginResponse($token);
    }

    private function getTokenByDto(Login $dto): string
    {
        $credentials = $this->getCredentialsFromDto($dto);

        /** @var JWTAuth $jwtAuth */
        $jwtAuth = app()->make(JWTAuth::class);
        $token = $jwtAuth->attempt($credentials);

        if (!$token) {
            throw new UnauthorizedException();
        }

        return $token;
    }

    private function getCredentialsFromDto(Login $dto): array
    {
        return [
            'email' => $dto->getEmail(),
            'password' => $dto->getPassword()
        ];
    }
}
