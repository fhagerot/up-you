<?php

declare(strict_types=1);


namespace Upper\Interfaces\Http\Api\Actions\Auth;


use Upper\Domain\Actions\Auth\Register;
use Upper\Interfaces\DataTransferObjects\Auth\Register as RegisterDto;

class RegisterAction
{
    public function __invoke(RegisterDto $dto, Register $register)
    {
        $register->call($dto);
    }
}
