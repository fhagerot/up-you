<?php

declare(strict_types=1);


namespace Upper\Interfaces\Http\Api\Actions\Activities;


use Upper\Domain\Actions\Activity;
use Upper\Interfaces\DataTransferObjects\Activities\AddActivity;

class AddAction
{
    public function __invoke(AddActivity $dto, Activity\Add $addActivity)
    {
        $addActivity->call($dto);
    }
}
