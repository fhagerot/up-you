<?php

declare(strict_types=1);


namespace Upper\Interfaces\Http\Api\Actions\Activities;


use Upper\Domain\Actions\Activity as ActivityActions;
use Upper\Domain\Entities\Activity\Activity;

class RemoveAction
{
    public function __invoke(Activity $activity, ActivityActions\Remove $removeActivity)
    {
        $removeActivity->call($activity->id);
    }
}
