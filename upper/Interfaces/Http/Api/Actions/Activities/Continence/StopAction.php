<?php

declare(strict_types=1);


namespace Upper\Interfaces\Http\Api\Actions\Activities\Continence;


use Upper\Domain\Actions\Activity\Continence;
use Upper\Domain\Entities\Activity\Activity;

class StopAction
{
    public function __invoke(Activity $activity, Continence\Stop $stop)
    {
        $stop->call($activity);
    }
}
