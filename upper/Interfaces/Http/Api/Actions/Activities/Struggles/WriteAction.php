<?php

declare(strict_types=1);


namespace Upper\Interfaces\Http\Api\Actions\Activities\Struggles;


use Upper\Domain\Actions\Activity\Struggle;
use Upper\Domain\Entities\Activity\Activity;
use Upper\Interfaces\DataTransferObjects\Activities\WriteStruggle;

class WriteAction
{
    public function __invoke(Activity $activity, WriteStruggle $dto, Struggle\Write $writeStruggle)
    {
        $writeStruggle->call($activity, $dto);
    }
}
