<?php

declare(strict_types=1);


namespace Upper\Interfaces\Http\Api\Actions\Activities;


use Illuminate\Contracts\Auth\Authenticatable;
use Upper\Domain\Repositories\ActivitiesRepository;
use Upper\Interfaces\Responses\Activities\ActivitiesListResponse;

class GetListAction
{
    public function __invoke(ActivitiesRepository $activities, Authenticatable $user)
    {
        return new ActivitiesListResponse($activities->getByUserId($user->id));
    }
}
