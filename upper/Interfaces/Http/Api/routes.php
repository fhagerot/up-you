<?php

use Illuminate\Support\Facades\Route;
use Upper\Interfaces\Http\Api\Actions\Activities;
use Upper\Interfaces\Http\Api\Actions\Auth;

Route::post('/login', Auth\LoginAction::class);
Route::post('/register', Auth\RegisterAction::class);

Route::group(['middleware' => 'auth:api'], function () {

    Route::prefix('/activities')->group(function () {
        Route::get('/', Activities\GetListAction::class);
        Route::post('/', Activities\AddAction::class);
        Route::prefix('/{activity}')->middleware('can:user-see,activity')->group(function () {

            Route::delete('/', Activities\RemoveAction::class);

            Route::prefix('/struggle')->group(function () {
                Route::post('/write', Activities\Struggles\WriteAction::class);
            });

            Route::prefix('/continence')->group(function () {
                Route::post('/start', Activities\Continence\StartAction::class);
                Route::post('/stop', Activities\Continence\StopAction::class);
            });

        });
    });

});
