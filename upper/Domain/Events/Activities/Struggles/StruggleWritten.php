<?php

declare(strict_types=1);


namespace Upper\Domain\Events\Activities\Struggles;


class StruggleWritten
{
    private $activityId;
    private $yes;

    public function __construct(int $activityId, $yes)
    {
        $this->activityId = $activityId;
        $this->yes = $yes;
    }
}
