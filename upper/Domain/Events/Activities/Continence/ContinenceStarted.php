<?php

declare(strict_types=1);


namespace Upper\Domain\Events\Activities\Continence;


class ContinenceStarted
{
    private $activityId;

    public function __construct(int $activityId)
    {
        $this->activityId = $activityId;
    }
}
