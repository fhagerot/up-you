<?php

declare(strict_types=1);


namespace Upper\Domain\Events\Activities;


class ActivityDeleted
{
    private $activityId;

    public function __construct(int $activityId)
    {
        $this->activityId = $activityId;
    }
}
