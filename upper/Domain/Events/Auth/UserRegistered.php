<?php

declare(strict_types=1);


namespace Upper\Domain\Events\Auth;


class UserRegistered
{
    private $userId;

    public function __construct(int $userId)
    {
        $this->userId = $userId;
    }
}
