<?php

declare(strict_types=1);


namespace Upper\Domain\Repositories;


use Upper\Domain\Entities\Activity\Activity;
use Upper\Domain\Entities\Activity\Types\ContinenceLog;

class ActivitiesRepository
{
    public function getByUserId(int $id)
    {
        return Activity::query()->where('user_id', $id)->get();
    }

    public function findByActivityIdAndUserId(int $activityId, int $userId)
    {
        return Activity::query()
            ->where([
                ['user_id', $userId],
                ['id', $activityId]
            ])
            ->first();
    }

    public function removeByActivityId(int $id)
    {
        Activity::query()->where('id', $id)->delete();
    }
}
