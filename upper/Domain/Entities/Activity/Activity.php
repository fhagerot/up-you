<?php

declare(strict_types=1);


namespace Upper\Domain\Entities\Activity;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Upper\Domain\Entities\Activity\Types\ContinenceLog;
use Upper\Domain\Entities\Activity\Types\ExerciseLog;
use Upper\Domain\Entities\Activity\Types\Struggle;
use Upper\Domain\Entities\Activity\Types\TimeExerciseLog;
use Upper\Domain\Entities\User;
use Upper\Domain\Exceptions\BusinessLogicException;

/**
 * @property int id
 * @property string name
 * @property string type
 * @property int user_id
 * @property Collection struggles
 * @property Collection exerciseLogs
 * @property Collection continenceLogs
 * @property Collection timeExerciseLogs
 */
class Activity extends Model
{
    public const TYPE_CONTINENCE = 'continence';
    public const TYPE_TIME_EXERCISE = 'time_exercise';
    public const TYPE_EXERCISE = 'exercise';
    public const TYPE_STRUGGLE = 'struggle';

    public const AVAILABLE_TYPES = [
        self::TYPE_CONTINENCE,
        self::TYPE_TIME_EXERCISE,
        self::TYPE_EXERCISE,
        self::TYPE_STRUGGLE
    ];

    protected $table = 'activities';

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function struggles()
    {
        return $this->hasMany(Struggle::class);
    }

    public function exerciseLogs()
    {
        return $this->hasMany(ExerciseLog::class);
    }

    public function continenceLogs()
    {
        return $this->hasMany(ContinenceLog::class);
    }

    public function timeExerciseLogs()
    {
        return $this->hasMany(TimeExerciseLog::class);
    }

    public function isStruggle(): bool
    {
        return $this->type === self::TYPE_STRUGGLE;
    }

    public function isContinence(): bool
    {
        return $this->type === self::TYPE_CONTINENCE;
    }

    public function isExercise(): bool
    {
        return $this->type === self::TYPE_EXERCISE;
    }

    public function isTimeExercise(): bool
    {
        return $this->type === self::TYPE_TIME_EXERCISE;
    }

    public function incrementStruggleYesOrNo(bool $yes): void
    {

        if (!$this->isStruggle()) {
            throw new BusinessLogicException('Wrong type of activity');
        }

        $struggle = $this->struggles()->first();

        if (!$struggle) {
            throw new BusinessLogicException('Struggle of activity not exists');
        }

        if ($yes) {
            $struggle->yes += 1;
        } else {
            $struggle->no += 1;
        }

        $struggle->save();
    }

    public function startContinence(): void
    {
        if (!$this->isContinence()) {
            throw new BusinessLogicException('Wrong type of activity');
        }

        $continenceLog = $this->getFirstContinenceLog();

        if ($continenceLog) {
            throw new BusinessLogicException('Continence already started');
        }

        $continenceLog = new ContinenceLog();
        $continenceLog->activity_id = $this->id;
        $continenceLog->save();
    }

    public function stopContinence(): void
    {
        if (!$this->isContinence()) {
            throw new BusinessLogicException('Wrong type of activity');
        }

        $continenceLog = $this->getFirstContinenceLog();

        if (!$continenceLog) {
            throw new BusinessLogicException('Continence log with null stopped at not found');
        }

        $continenceLog->stopped_at = now();
        $continenceLog->save();
    }

    /**
     * @return ContinenceLog
     */
    private function getFirstContinenceLog(): ContinenceLog
    {
        $this->load(['continenceLogs' => function ($query) {
            $query->where('stopped_at', null);
        }]);

        /** @var ContinenceLog $continenceLog */
        $continenceLog = $this->continenceLogs->first();
        return $continenceLog;
    }
}
