<?php

declare(strict_types=1);


namespace Upper\Domain\Entities\Activity\Types;


use Illuminate\Database\Eloquent\Model;

/**
 * @property int id
 * @property int activity_id
 * @property int count
 * @property \DateTimeInterface created_at
 * @property \DateTimeInterface updated_at
 */
class ExerciseLog extends Model
{
    protected $table = 'exercise_logs';
}
