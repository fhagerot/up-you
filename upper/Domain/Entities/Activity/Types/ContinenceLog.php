<?php

declare(strict_types=1);


namespace Upper\Domain\Entities\Activity\Types;


use Illuminate\Database\Eloquent\Model;

/**
 * @property int id
 * @property int activity_id
 * @property \DateTimeInterface|null stopped_at
 * @property \DateTimeInterface created_at
 * @property \DateTimeInterface updated_at
 */
class ContinenceLog extends Model
{
    protected $table = 'continence_logs';
}
