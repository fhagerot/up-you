<?php

declare(strict_types=1);


namespace Upper\Domain\Entities\Activity\Types;


use Illuminate\Database\Eloquent\Model;

/**
 * @property int id
 * @property int activity_id
 * @property \DateTimeInterface start
 * @property \DateTimeInterface stop
 * @property \DateTimeInterface created_at
 * @property \DateTimeInterface updated_at
 */
class TimeExerciseLog extends Model
{
    protected $table = 'time_exercise_logs';
}
