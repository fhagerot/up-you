<?php

declare(strict_types=1);


namespace Upper\Domain\Factories;

use Illuminate\Support\Facades\Hash;
use Upper\Domain\Entities\User;
use Upper\Interfaces\DataTransferObjects\Auth\Register;

class UserFactory
{
    public function createUserByRegisterDto(Register $dto): User
    {
        $user = new User();

        $user->name = $dto->getName();
        $user->email = $dto->getEmail();
        $user->password = Hash::make($dto->getPassword());

        $user->save();

        return $user;
    }
}
