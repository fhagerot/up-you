<?php

declare(strict_types=1);


namespace Upper\Domain\Factories;

use Upper\Domain\Entities\Activity\Activity;
use Upper\Domain\Entities\Activity\Types\Struggle;
use Upper\Interfaces\DataTransferObjects\Activities\AddActivity;

class ActivityFactory
{
    public function createActivityByDtoAndUserId(AddActivity $dto, int $userId): Activity
    {
        $activity = new Activity();

        $activity->name = $dto->getName();
        $activity->user_id = $userId;
        $activity->type = $dto->getType();
        $activity->save();

        switch ($activity->type) {
            case Activity::TYPE_STRUGGLE:
                $struggle = new Struggle();
                $activity->struggles()->save($struggle);
                break;
        }

        return $activity;
    }
}
