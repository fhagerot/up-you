<?php

declare(strict_types=1);


namespace Upper\Domain\Actions\Auth;


use Illuminate\Contracts\Events\Dispatcher;
use Upper\Domain\Events\Auth\UserRegistered;
use Upper\Domain\Factories\UserFactory;
use Upper\Interfaces\DataTransferObjects\Auth\Register as RegisterDto;

class Register
{
    private $userFactory;

    private $events;

    public function __construct(UserFactory $userFactory, Dispatcher $events)
    {
        $this->userFactory = $userFactory;
        $this->events = $events;
    }

    public function call(RegisterDto $dto)
    {
        $user = $this->userFactory->createUserByRegisterDto($dto);

        $this->events->dispatch(new UserRegistered($user->id));
    }
}
