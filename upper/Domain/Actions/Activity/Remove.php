<?php

declare(strict_types=1);


namespace Upper\Domain\Actions\Activity;


use Illuminate\Contracts\Events\Dispatcher;
use Upper\Domain\Events\Activities\ActivityDeleted;
use Upper\Domain\Repositories\ActivitiesRepository;

class Remove
{
    private $events;

    private $activities;

    public function __construct(Dispatcher $events, ActivitiesRepository $activities)
    {
        $this->events = $events;
        $this->activities = $activities;
    }

    public function call($activityId)
    {
        $this->activities->removeByActivityId($activityId);

        $this->events->dispatch(new ActivityDeleted($activityId));
    }
}
