<?php

declare(strict_types=1);


namespace Upper\Domain\Actions\Activity\Struggle;


use Illuminate\Contracts\Events\Dispatcher;
use Upper\Domain\Entities\Activity\Activity;
use Upper\Domain\Events\Activities\Struggles\StruggleWritten;
use Upper\Interfaces\DataTransferObjects\Activities\WriteStruggle;

class Write
{
    private $events;

    public function __construct(Dispatcher $events)
    {
        $this->events = $events;
    }

    public function call(Activity $activity, WriteStruggle $dto): void
    {
        $activity->incrementStruggleYesOrNo($dto->getYes());

        $this->events->dispatch(new StruggleWritten($activity->id, $dto->getYes()));
    }
}
