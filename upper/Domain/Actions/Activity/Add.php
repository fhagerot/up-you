<?php

declare(strict_types=1);


namespace Upper\Domain\Actions\Activity;


use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Events\Dispatcher;
use Upper\Domain\Entities\User;
use Upper\Domain\Events\Activities\NewActivityCreated;
use Upper\Domain\Factories\ActivityFactory;
use Upper\Interfaces\DataTransferObjects\Activities\AddActivity;

class Add
{
    private $activityFactory;

    /** @var User */
    private $user;

    private $events;

    public function __construct(ActivityFactory $activityFactory, Authenticatable $user, Dispatcher $events)
    {
        $this->activityFactory = $activityFactory;
        $this->user = $user;
        $this->events = $events;
    }

    public function call(AddActivity $dto)
    {
        $activity = $this->activityFactory->createActivityByDtoAndUserId($dto, $this->user->id);

        $this->events->dispatch(new NewActivityCreated($activity->id));
    }
}
