<?php

declare(strict_types=1);


namespace Upper\Domain\Actions\Activity\Continence;


use Illuminate\Contracts\Events\Dispatcher;
use Upper\Domain\Entities\Activity\Activity;
use Upper\Domain\Events\Activities\Continence\ContinenceStarted;

class Start
{
    private $events;

    public function __construct(Dispatcher $events)
    {
        $this->events = $events;
    }

    public function call(Activity $activity): void
    {
        $activity->startContinence();

        $this->events->dispatch(new ContinenceStarted($activity->id));
    }
}
