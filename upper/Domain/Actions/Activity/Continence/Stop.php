<?php

declare(strict_types=1);


namespace Upper\Domain\Actions\Activity\Continence;


use Illuminate\Contracts\Events\Dispatcher;
use Upper\Domain\Entities\Activity\Activity;
use Upper\Domain\Events\Activities\Continence\ContinenceStopped;
use Upper\Domain\Repositories\ActivitiesRepository;

class Stop
{
    private $events;

    private $activities;

    public function __construct(ActivitiesRepository $activities, Dispatcher $events)
    {
        $this->events = $events;
        $this->activities = $activities;
    }

    public function call(Activity $activity): void
    {
        $activity->stopContinence();

        $this->events->dispatch(new ContinenceStopped($activity->id));
    }
}
