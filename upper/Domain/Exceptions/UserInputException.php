<?php

declare(strict_types=1);

namespace Upper\Domain\Exceptions;


class UserInputException extends BaseException
{
    private $errors;

    protected $code = 422;

    public function __construct($errors, $message = "")
    {
        parent::__construct($message);

        $this->errors = $errors;
    }

    public function getErrors()
    {
        return $this->errors;
    }

}
