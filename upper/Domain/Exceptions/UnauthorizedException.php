<?php

declare(strict_types=1);

namespace Upper\Domain\Exceptions;


class UnauthorizedException extends BaseException
{
    protected $code = 401;

    protected $message = 'Unauthorized';
}
