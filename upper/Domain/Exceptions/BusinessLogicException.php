<?php


namespace Upper\Domain\Exceptions;


class BusinessLogicException extends BaseException
{
    protected $code = 422;
}

