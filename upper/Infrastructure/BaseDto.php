<?php

declare(strict_types=1);


namespace Upper\Infrastructure;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Upper\Domain\Exceptions\UserInputException;

abstract class BaseDto
{

    public abstract function rules(): array;

    public function __construct(Request $request)
    {
        $params = $request->all();

        $this->validate($params);

        $this->setProperties($params);
    }

    private function validate(array $params): void
    {
        $validator = Validator::make($params, $this->rules());

        if ($validator->fails()) {
            throw new UserInputException($validator->errors(), 'Validation failed');
        }
    }

    private function setProperties(array $params):void
    {
        foreach ($params as $key => $value) {
            if (!property_exists($this, $key) || $value === null) {
                continue;
            }
            $method_name = 'set' . ucfirst(Str::camel($key));
            if (method_exists($this, $method_name)) {
                call_user_func(array($this, $method_name), $value);
            }
        }
    }
}
