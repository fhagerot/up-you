<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;
use Upper\Domain\Exceptions\UnauthorizedException;
use Upper\Domain\Exceptions\UserInputException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {

        $this->renderable(function (UserInputException $exception, Request $request) {

            if ($request->wantsJson()) {
                return new JsonResponse(
                    $exception->getErrors(),
                    $exception->getCode(), [],
                    JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES
                );
            }

            return response($exception->getErrors(), $exception->getCode());
        });

        $this->renderable(function (UnauthorizedException $exception, Request $request) {

            if ($request->wantsJson()) {
                return new JsonResponse(
                    [
                        'error' => $exception->getMessage()
                    ],
                    $exception->getCode()
                );
            }

            return response(
                [
                    'error' =>$exception->getMessage()
                ],
                $exception->getCode()
            );
        });

        $this->renderable(function (NotFoundHttpException $exception, Request $request) {
            if ($request->wantsJson()) {
                return new JsonResponse(
                    [
                        'error' => $exception->getMessage()
                    ],
                    $exception->getStatusCode()
                );
            }

            return response(
                [
                    'error' => $exception->getMessage()
                ],
                $exception->getStatusCode()
            );
        });

        $this->reportable(function (Throwable $e) {

        });
    }
}
